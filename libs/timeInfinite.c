#include <time.h>
#include <stdio.h>
#include <unistd.h>


typedef void (*taskPtr)(void);



void zadanie5(void) 
{
	time_t now;
	int cnt = 0;
	while(1) 
	{
		sleep(1);
		time(&now);
		printf("Zadanie5 : %s", ctime(&now));
		cnt++;
	}
}

taskPtr getTask()
{
    return &zadanie5;
}



