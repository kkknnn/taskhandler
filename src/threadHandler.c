#define _GNU_SOURCE
#include "threadHandler.h"
#include "libLoader.h"
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <pthread.h>

static threadHandlerType threadArr[MAX_THREAD_NUMBER]={{0}};
static uint8_t currentIndex = 0;

void * threadWrapper(void * thr)
{
    threadArr[currentIndex].pid = gettid();
    taskPtr task = (taskPtr)thr;
    (*task)();
}

static boolean isThreadAlive(uint8_t id)
{
    return (0 == pthread_kill(threadArr[id].id,0)) ? TRUE : FALSE;
}

static boolean findFreeThreadIndex(uint8_t * freeIndex)
{
    boolean retVal = TRUE;
    uint8_t index = 0;
    while(0 != threadArr[index].id)
    {
        index++;
        if(MAX_THREAD_NUMBER-1 == index)
        {
            retVal = FALSE;

        }
    }
    *freeIndex = index;
    return retVal;
}

static boolean findUnusedThreadIndex(uint8_t * freeIndex)
{
    boolean retVal = TRUE;
    uint8_t index = 0;
    while(0 != pthread_kill(threadArr[index].id,0))
    {
        index++;
        if(MAX_THREAD_NUMBER-1 == index)
        {
            retVal = FALSE;
        }
    }
    *freeIndex = index;
    return retVal;
}


int runThread(uint8_t id)
{

    pthread_t thread;
    int  iret1 = 1;
    uint8_t index = 0;

    taskPtr taskToStart = retriveTaskPtr(id);
    if(taskToStart != NULL)
    {
        if(findFreeThreadIndex(&index) || findUnusedThreadIndex(&index))
        {
            currentIndex = index;
            iret1 = pthread_create(&thread, NULL,&threadWrapper,taskToStart);
            if(iret1 != 0)
            {
                printf("Error in creating thread \n");
            }
            else
            {
                retriveThreadInfo(id,&threadArr[index]);
                threadArr[index].id = thread;
                time(&threadArr[index].starttime);
                printf("Task successfuly started\n");
            }
        }
        else
        {
            printf("Error, maximum number of started threads exceeded\n");
        }


    }
    else
    {
        printf("Wrong task ID\n");
    }
    return  iret1;
}

void showRunningTasks()
{
    printf("\n***ACTIVE TASKS***\n");
    printf("ID \t Name \t\t Lib \t\t\t start time\n");
    for(int i = 0; i<MAX_THREAD_NUMBER; i++)
    {
        if (0!= threadArr[i].id && 0 == pthread_kill(threadArr[i].id,0))
        {
            printf("%d \t %s \t %s \t %s \n",i,threadArr[i].taskName,
                   threadArr[i].libName, ctime(&threadArr[i].starttime));
        }
    }
}

void showAllTasksStatus()
{
    printf("\n***TASKS HISTORY***\n");
    printf("ID \t name \t\t lib \t\t\t start time \n");
    for(int i = 0; i<MAX_THREAD_NUMBER; i++)
    {
        if (0!= threadArr[i].id)
        {
            if(0 == pthread_kill(threadArr[i].id,0))
            {

                printf("%d \t %s \t %s \t %s \t %s",i,threadArr[i].taskName,
                       threadArr[i].libName, ctime(&threadArr[i].starttime),"ACTIVE\n");
            }
            else
            {

                printf("%d \t %s \t %s \t %s \t %s",i,threadArr[i].taskName,
                       threadArr[i].libName, ctime(&threadArr[i].starttime),"NOT ACTIVE\n");            }
        }
    }
}

int cancelThread(uint8_t id)
{
    if(0!=threadArr[id].id)
    {
        if(0 == pthread_kill(threadArr[id].id,0))
        {
            if(0!= pthread_cancel(threadArr[id].id))
            {
                printf("Something went wrong, try again later \n");
            }
            else
            {
                printf("Task deactivated \n");
            }
        }
        else
        {
            printf("Error - task is not active \n");
        }
    }
    else
    {
        printf("Wrong ID number\n");
    }
}

int getSystemProcessId(uint8_t id)
{
    int retVal = 0;
    if(0 != threadArr[id].pid)
    {
        retVal = threadArr[id].pid;
    }
    return retVal;
}

void cancellAllTasks()
{
    for(int i = 0; i<MAX_THREAD_NUMBER; i++)
    {
        if (0!= threadArr[i].id && 0 == pthread_kill(threadArr[i].id,0))
        {
            pthread_cancel(threadArr[i].id);
        }
    }
}
