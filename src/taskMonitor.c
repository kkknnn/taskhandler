#define _GNU_SOURCE

#include "taskMonitor.h"
#include <stdio.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <sys/resource.h>
#include <pthread.h>
#include <string.h>
#include <stdlib.h>
#include "threadHandler.h"
#include <sys/sysinfo.h>
#include <dirent.h>

static pid_t getMainTaskId(void) ;
static void getTaskSystemInfo(pid_t taskId) ;
static boolean monitoringActive = FALSE;
static int numOfProcessors =0;

typedef struct
{

    unsigned long long VMS;
    unsigned long long RSS;

}memoryInfo;

void * threadMonitorWrapper(void * thr)
{
    pid_t* id = ((pid_t*)thr);

    monitoringActive = TRUE;

    while (monitoringActive)
    {
       getTaskSystemInfo(*id);
       usleep(100000);
    }
    free(thr);
}

pthread_t thread_proc(uint8_t taskId)
{
    pthread_t id = 0;
    pid_t procesId = getSystemProcessId(taskId);
    if(0 != procesId)
    {
    pid_t * arg = malloc(sizeof(*arg)); //freed in wrapper
    *arg = procesId;
    pthread_create(&id,0,threadMonitorWrapper,arg);
    }
    else
    {
        printf("Wrong ID \n");
    }
    return id;
}

pid_t getMainTaskId(void) {
    pid_t tid = syscall(SYS_gettid);
    return tid ;
}


static unsigned long long getSystemTime(void)
{
    unsigned long long sysTime = 0;
    unsigned long long user, nice, system, idle, time1, time2, time3 = 0;
    char *line = 0;
    long unsigned line_size = 0;
    char sysfname[] = "/proc/stat";
    FILE *sfp = fopen(sysfname, "r") ;
    if(sfp)
    {
        getline(&line,&line_size,sfp);

        sscanf(line,"%*s %llu %llu %llu %llu %llu %llu %llu",&user,&nice,&system,&idle, &time1, &time2, &time3);
        fclose(sfp);

    }
    else
    {
        printf("Error in reading system info \n");
    }

    sysTime = user + nice + system + idle + time1 + time2 + time3;
    return sysTime;

}

long long getProcessTime(pid_t taskId)
{
    char fname[50];
    snprintf(fname, sizeof(fname), "/proc/%d/stat", taskId) ;
    FILE *fp = fopen(fname, "r") ;
    long long usertime=0;
    long long systemtime=0;
    long long childtime1 = 0;
    long long childtime2 = 0;
    char *line = 0;
    long unsigned line_size = 0;
    if(fp != 0)
    {
        getline(&line,&line_size,fp);
        sscanf(line, "%*s %*s %*c %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %lld %lld %lld %lld %*s %*s %*s %*s %*s %*d",
        &usertime,&systemtime, &childtime1, &childtime2);
        fclose(fp);

    }

    return usertime + systemtime + childtime1 + childtime2;

}


static void getTaskState(char * mappedState, FILE * fp)
{

    char state =' ';

    if (
        fscanf(fp, "%*s %*s %c",
        &state) == 1)
        switch (state)
        {
        case 'R':
        {
            strcpy(mappedState,"RUNNING");
            break;
        }
        case 'S':
        {
            strcpy(mappedState,"SLEEPING");
            break;
        }
        case 'Z':
        {
            strcpy(mappedState,"ZOMBIE");
            break;
        }
        case 'X':
        {
            strcpy(mappedState,"DEAD");
            break;
        }
        default:
        {
            strcpy(mappedState,"UNDEFINED");
            break;
        }
    }

}


static memoryInfo memoryUsage(pid_t taskId)
{
    long page_size_kb = sysconf(_SC_PAGE_SIZE) / 1024;
    memoryInfo memInfo;
    char *line = 0;
    long unsigned line_size = 0;
    char fname[50];
    snprintf(fname, sizeof(fname), "/proc/%d/statm", taskId) ;
    FILE *fp = fopen(fname, "r") ;
    if(fp)
    {
        getline(&line,&line_size,fp);
        sscanf(line, "%lld %lld",
        &memInfo.VMS, &memInfo.RSS);
        fclose(fp);

    }
    memInfo.VMS=memInfo.VMS * page_size_kb;
    memInfo.RSS=memInfo.RSS * page_size_kb;

    return memInfo;

}

void getTaskSystemInfo(pid_t taskId)
{

    static uint8_t tickCounter = 0;
    static float totalCPU = 0;
    static uint8_t stuckCounter = 0;
    static boolean stucked = FALSE;
    static long long sysTime1, sysTime2, procTime1, procTime2 = 0;
    char taskState[12];
    char fname[50];
    snprintf(fname, sizeof(fname), "/proc/%d/stat", taskId) ;
    FILE *fp = fopen(fname, "r") ;
    if ( !fp )
    {
        snprintf(fname, sizeof(fname), "/proc/%d/", taskId) ;
        DIR* dir = opendir("mydir");
        if(!dir)
        {
            strcpy(taskState, "TERMINATED");
            printf("\rSTATE: %s CPU USAGE: N/A VMS: N/A RSS: N/A", taskState);
            fflush(stdout);
        }
        else
        {
            printf("Process monitoring error \n");
        }
    }
    else
    {
    getTaskState(taskState, fp);
    fclose(fp) ;

    if(0 == tickCounter)
    {
        sysTime1 = getSystemTime();
        procTime1 = getProcessTime(taskId);
    }
    else if(5 == tickCounter)
    {
        sysTime2 = getSystemTime();
        procTime2 = getProcessTime(taskId);

    }
    tickCounter++;
    if(10 <= tickCounter)
    {
        totalCPU = ((double)(get_nprocs()*((procTime2 - procTime1)))) / ((double) ( sysTime2 - sysTime1 ));
        if(!strcmp(taskState,"RUNNING") && totalCPU>0.99)
        {
            if(stuckCounter>=2)
            {
                stucked = TRUE;
            }
            else
            {
                stuckCounter++;
            }
        }
        else
        {
            stucked = FALSE;
            stuckCounter = 0;
        }
        tickCounter = 0;
    }

    if(FALSE != stucked)
    {
        strcpy(taskState,"STUCKED");
    }
    memoryInfo usage = memoryUsage(taskId);
    fflush(stdout);
    printf("\r STATE: %s CPU USAGE: %d %% VMS: %llu RSS: %llu             ",
           taskState,(int)(totalCPU*100),usage.VMS, usage.RSS);
    fflush(stdout);
    }
}

void turnOffMonitoring()
{
    monitoringActive = FALSE;
    usleep(100);
    /*clear terminal line*/
    printf("\r                                    \r");
    fflush(stdout);
}
