#gcc *.c -c -fPIC
#gcc *.o -shared -o mylib.so
#gcc third.c -c -fPIC
#gcc third.o -shared -o mylib.so

for f in *.c
do
  filename=$(basename -- "$f")
  filename="${filename%.*}"
  gcc ${f} -c -fPIC
  gcc ${filename}.o -shared -o lib${filename}.so
done
