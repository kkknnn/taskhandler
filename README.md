**Task Handler @ Krzysztof Nogaj**

Recruitment project for I. company.

*Tematem zadania jest program który umożliwia uruchamianie zadań. Zadania te powinny być
uruchamiane w oddzielnych wątkach, tak aby możliwe było monitorowanie ich oraz zakańczanie. To
samo zadanie może być uruchomione wiele razy, zależy to od użytkownika.*

---

## Creating local copy of repository

1. Open terminal.
2. Install git if needed by entering **sudo apt-get install git-core**.
3. Navigate to desired location.
4. Enter command **git clone https://kkknnn@bitbucket.org/kkknnn/taskhandler.git**

---

## Build & Run

1. Navigate to main directory
2. Run **install.sh** script by typing "./install.sh"
3. Enter super user password if prompted.
4. If you can't run script, modify its access rights by entering command "chmod +rwx install.sh".

*Script checks for following project dependencies:

- GCC compiler
- cmake
- pthread library

and installs them if needed.

It also creates bin folder, if not present, generates makefile with cmake, builds program in bin directory and runs it.
*

---


## Using libraries

1. Program scans **lib/** folder and searches for files with **.so** extensions. 
2. Library shall implement **getTask** function as API - simple getter returning pointer to task function (void type and no arguments).
3. You can use ./lib/first.c as template for your own implementation.
4. After implementing .c files, **./lib/makelibs.sh** script can be used to generate libs
5. It creates shared libraries for each of *.c files present in lib/ folder with naming pattern **lib{c_filename}.so**. 
6. Wrong library implementation/construction is signalised by application at startup i.e.:

![picture](doc/libError.png)

Don't worry. Application will continue despite malfunctioned library.

## Program description

![picture](doc/mainmenu.png)

After startup, program shows main menu. Type one of following numbers to choose option:

1) You’ll see the list of tasks possible to be runned. Type ID of task to start it.

![picture](doc/RunTask.png)

2) You’ll see the list of currently active tasks. Type enter to return to main menu.

![picture](doc/ActiveTasks.png)

3) You’ll see the list of runned tasks since startup. This list includes terminated/finshed tasks.  Type enter to return to main menu.

![picture](doc/AllTasks.png)

4) You’ll see the list of active tasks. Type task ID to be cancelled or 'r' to return to main menu (or any other literal character).

![picture](doc/TaskCancel.png)


5) You’ll see the list runned tasks since startup. Type task ID to start real time monitoring.
![picture](doc/terminated2.png)

6) Exit.


## Program general info
1. It uses linux built-in dynamic linking loader (dlopen) to dynamicly load libs to RAM during runtime and retrive nessesary information.
2. It implements own thread management system.
3. It uses pthread lib to run/stop threads.
3. Thread number running in parallel is limited to 255, library number is unlimited.


##  UPDATE (24.01.21)
Application introduces real time task monitoring feature (watchdog). It uses /proc filesystem that is a pseudo-filesystem which provides an interface to kernel data structures. 
It retrieves information about task state, CPU usage and memory consumption for debug purposes. 
Its 4 main states are: 

1) running (try with libcounting.so) - normal state, task is working

![picture](doc/running.png)

2) sleeping (try with libcounting.so) - task is in an interruptible wait (i.e. usleep())

![picture](doc/sleeping.png)

3) stucked (try with libidle.so)- task probably hanged i.e. in endless loop not performing any calculations. This state is recognized by 3 seconds of consecutive near full(99%+) CPU consumption.
![picture](doc/stucked.png)

4) terminated - task is not performing any actions, because it was cancelled / ended its job

![picture](doc/terminated.png)

App is also able to read other linux process states i.e. zombie.

Task monitoring also provides CPU consumption information. It is calculated every 1s (2 measures are needed, each is performed in 0.5s intervals). It also provides 2 basic information about memory consumption in kB:

VSZ is the Virtual Memory Size. It includes all memory that the process can access, including memory that is swapped out, memory that is allocated, but not used, and memory that is from shared libraries.
RSS is the Resident Set Size and is used to show how much memory is allocated to that process and is in RAM. It does not include memory that is swapped out. It does include memory from shared libraries as long as the pages from those libraries are actually in memory. It does include all stack and heap memory.