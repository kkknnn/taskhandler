#include <regex.h>
#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>
#include "menu.h"
#include <sys/syscall.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>


int main()
{
    /* init */
    countLibs(PATH_TO_LIBS);
    listDirectory(PATH_TO_LIBS);
    addTasks();
    /* program loop */
    while(showMenu())
    {
    }
    /* deinit */
    cancellAllTasks();
    freeLibs();
    printf("Bye\n");

    return 0;
}
