#!/bin/bash
if [ $(dpkg-query -W -f='${Status}' build-essential 2>/dev/null | grep -c "ok installed") -eq 0 ];
then
    sudo apt-get install build-essential -y
else
    echo "found gcc"
fi

if [ $(dpkg-query -W -f='${Status}' cmake 2>/dev/null | grep -c "ok installed") -eq 0 ];
then
    sudo apt-get install cmake -y
else
    echo "found cmake"
fi

if [ $(dpkg-query -W -f='${Status}' libpthread-stubs0-dev 2>/dev/null | grep -c "ok installed") -eq 0 ];
then
    sudo apt-get install libpthread-stubs0-dev -y
else
    echo "found pthread"
fi

BUILD_DIR="bin/"
if [ ! -d "$BUILD_DIR" ]; then
  echo "creating ${BUILD_DIR}"
  mkdir -p "$BUILD_DIR"
fi
cd ${BUILD_DIR}
cmake ..
make
./taskhandler
