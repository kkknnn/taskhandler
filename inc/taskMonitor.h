#ifndef TASK_MONIOTOR
#define TASK_MONIOTOR
#include <pthread.h>
#include <stdint.h>

pthread_t thread_proc(uint8_t taskId);

void turnOffMonitoring();

#endif
