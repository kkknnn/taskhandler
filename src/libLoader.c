#define _GNU_SOURCE
#include <dirent.h>
#include "libLoader.h"
#include <stdio.h>
#include <stdlib.h>
#include <regex.h>
#include <dlfcn.h>
#include <string.h>

#define NUM_TASKS 10


static void** libArr;
static taskHandlerType* taskArr = NULL;
static uint8_t libNumber = 0;
static uint8_t libCounter = 0;
static uint8_t taskCounter = 0;
static uint8_t taskNumber = 0;


void fillLibArr(const char * filename)
{
    if(libCounter < libNumber)
    {
        char path[MAX_PATH_LENGTH] = PATH_TO_LIBS;
        strcat(path,filename);
        void * libHdl = openLib(path);
        libArr[libCounter]=libHdl;
        libCounter++;
    }
}




void listDirectory(const char *directory)
{
    DIR *d = NULL;
    struct dirent *dir = NULL;
    regex_t regex;
    int retVal = 0;
    retVal = regcomp(&regex, ".*\\.so$", 0);
    if(retVal != 0)
    {
        printf("Error in regex compilation");
        exit(EXIT_FAILURE);
    }

    d = opendir(directory);
    if (d != NULL)
    {
      while ((dir = readdir(d)) != 0)
      {
       retVal = regexec(&regex, dir->d_name, 0, NULL, 0);
       if (!retVal)
       {
           fillLibArr(dir->d_name);

       }
      }
      closedir(d);
    }
}

void *openLib(const char* path)
{
    void * libHandle = dlopen(path, RTLD_LAZY);
    if (libHandle == NULL)
    {
        printf("Error: %s\n", dlerror());
        exit(EXIT_FAILURE);
    }
    return libHandle;
}

taskPtr getTaskPtr(void *libHdl)
{
    taskPtr tsk = NULL;
    getterPtr ptr=(getterPtr)dlsym(libHdl, "getTask");
    if(ptr!=NULL)
    {
        tsk = (*ptr)();
        if(tsk!=NULL)
        {
            Dl_info info;
            dladdr(tsk,&info);
            taskArr[taskCounter].ptr = tsk;
            taskArr[taskCounter].libName=info.dli_fname;
            taskArr[taskCounter].taskName=info.dli_sname;
            taskCounter++;
        }

    }
    else
    {
        printf("Error: %s\n", dlerror());
    }
    return tsk;
}

void freeLibs(void)
{
    for(uint8_t i = 0; i<libCounter; i++)
    {
        dlclose(libArr[i]);
    }
    if(taskArr)
    {
        free(taskArr);
    }
    if(libArr)
    {
        free(libArr);
    }
}

void countLibs(const char *directory)
{
    DIR *d = NULL;
    struct dirent *dir = NULL;
    regex_t regex;
    int retVal = 0;
    retVal = regcomp(&regex, ".*\\.so$", 0);
    if(retVal != 0)
    {
        printf("Error in regex compile");
        EXIT_FAILURE;
    }
    d = opendir(directory);
    if (d != NULL)
    {
      while ((dir = readdir(d)) != 0)
      {
       retVal = regexec(&regex, dir->d_name, 0, NULL, 0);
       if (!retVal)
       {
           libNumber++;
       }
      }
      closedir(d);
    }
    libArr = calloc(libNumber, sizeof(void*));
    taskArr = calloc(libNumber, sizeof(taskHandlerType));
}

void addTasks(void)
{
    for(uint8_t i = 0; i<libCounter; i++)
    {
        getTaskPtr(libArr[i]);
    }
}

taskPtr retriveTaskPtr(uint8_t arrId)
{   taskPtr retTask = NULL;
    if(taskCounter > arrId)
    {
        retTask = taskArr[arrId].ptr;
    }
    return retTask;
}

void printTasks(void)
{
    for(uint8_t i = 0; i<taskCounter; i++)
    {
        printf("ID: %d\t NAME: %s\t FROM: %s\n",i,taskArr[i].taskName,taskArr[i].libName);
    }
}

void retriveThreadInfo(uint8_t arrId, threadHandlerType *threadInfo)
{
    if(arrId<taskCounter)
    {
    threadInfo->libName = taskArr[arrId].libName;
    threadInfo->taskName = taskArr[arrId].taskName;
    }
}
