#ifndef THREAD_HANDLER
#define THREAD_HANDLER

#include <stdint.h>

#define MAX_THREAD_NUMBER 255
#ifndef FALSE
#define FALSE 0u
#endif
#ifndef TRUE
#define TRUE 1u
#endif

typedef unsigned char boolean;

int runThread(uint8_t id);

int cancelThread(uint8_t id);

void showRunningTasks();

int getSystemProcessId(uint8_t id);

void showAllTasksStatus();

void cancellAllTasks();

#endif
