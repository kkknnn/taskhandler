#ifndef LIBLOADER
#define LIBLOADER

#include <pthread.h>
#include <stdint.h>

#ifndef NULL
#define NULL 0
#endif

#define PATH_TO_LIBS "../libs/"
#define MAX_PATH_LENGTH 50

typedef void (*taskPtr)(void);
typedef taskPtr (*getterPtr)(void);
typedef struct
{
    taskPtr ptr;
    const char *taskName;
    const char *libName;
} taskHandlerType;

typedef struct
{
    pthread_t id;
    pid_t pid;
    const char *taskName;
    const char *libName;
    time_t starttime;
} threadHandlerType;

taskPtr retriveTaskPtr(uint8_t arrId);

void retriveThreadInfo(uint8_t arrId, threadHandlerType *threadInfo);

void fillLibArr(const char *filename);

void countLibs(const char *directory);

void listDirectory(const char *directory);

void addTasks(void);

void *openLib(const char *path);

taskPtr getTaskPtr(void *libHdl);

void printTasks(void);

void freeLibs(void);

#endif
