#include "menu.h"
#include <stdio.h>
#include "taskMonitor.h"
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <signal.h>
#include <string.h>
#include <stdlib.h>

#define MENU_ERR_VALUE 0xFFFFu

static inline void cleanInBuffer(void)
{
    while ( getchar() != '\n' ); //clean stdin buffer
}

static void runTaskMenu(void)
{
    uint16_t menuOption = MENU_ERR_VALUE;
    printTasks();
    printf("\nType ID of task to be started or 'r' to return\n");
    scanf("%hd",&menuOption);
    cleanInBuffer();
    if(MENU_ERR_VALUE!=menuOption)
    {
         runThread((uint8_t)menuOption);
    }
}

static void showActiveTaskMenu(void)
{
    showRunningTasks();
    printf("\nPress enter to continue\n");
    getchar();
}

static void showAllTaskMenu(void)
{
    showAllTasksStatus();
    printf("\nPress enter to continue\n");
    getchar();
}

static void showTaskMonitor(void)
{
    uint16_t menuOption = MENU_ERR_VALUE;
    showAllTasksStatus();
    pthread_t id = 0;
    printf("\nType ID of task to start monitoring or 'r' to return\n");
    scanf("%hd",&menuOption);
    cleanInBuffer();
    if(MENU_ERR_VALUE!=menuOption)
    {
        id = thread_proc((uint8_t)menuOption);
    }
    getchar();
    turnOffMonitoring();
    if(0!=id)
    {
        pthread_join(id,0);
    }
}

static void showCancelTaskMenu(void)
{
    uint16_t menuOption = MENU_ERR_VALUE;
    showRunningTasks();
    printf("\nType ID of task to be cancelled\n");
    scanf("%hd",&menuOption);
    cleanInBuffer();
    if(menuOption!=MENU_ERR_VALUE)
    {
        cancelThread((uint8_t)menuOption);
    }
}

boolean showMenu(void)
{
    boolean retVal = TRUE;
    uint16_t menuOption = 0;
    printf("\n***Menu***\n");
    printf("1.Run task\n");
    printf("2.Show active tasks\n");
    printf("3.Show all tasks\n");
    printf("4.Cancel active task\n");
    printf("5.Task monitor\n");
    printf("6.Exit\n");
    printf("\nChoose option\n");
    scanf("%hd",&menuOption);
    cleanInBuffer();
    switch(menuOption)
    {
        case 1:
        {
            runTaskMenu();
            break;
        };
        case 2:
        {
            showActiveTaskMenu();
            break;
        };
        case 3:
        {
            showAllTaskMenu();
            break;
        };
        case 4:
        {
            showCancelTaskMenu();
            break;
        };
        case 5:
        {
            showTaskMonitor();
            break;
        };
        case 6:
        {
            retVal = FALSE;
            break;
        };
        default:
        {
         printf("Please select correct menu option\n");
        }

    }

    return retVal;
}
